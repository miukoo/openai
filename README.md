# 简介 openai
快速调用OpenAi官方 GPT-3 Api的Java库。Java libraries for using OpenAI's GPT-3 api.



# 提供APIs Supported APIs
> 更多的功能后续会持续提供
- [Models](https://platform.openai.com/docs/api-reference/models)
- [Completions](https://platform.openai.com/docs/api-reference/completions)
- [Chat Completions](https://platform.openai.com/docs/api-reference/chat/create)
- [Edits](https://platform.openai.com/docs/api-reference/edits)
- [Embeddings](https://platform.openai.com/docs/api-reference/embeddings)
- [Files](https://platform.openai.com/docs/api-reference/files)
- [Images](https://platform.openai.com/docs/api-reference/images)
- [Moderations](https://platform.openai.com/docs/api-reference/moderations)


# 特色Feature

- 底层使用OkHttp封装并启用连接池，请求<b style="color:red">效率高</b>
- 底层<b style="color:red">超时参数</b>可以<b style="color:red">动态调整</b>
- <b style="color:red">自动识别本机的常用代理设置</b>



# 导入 Importing

```xml
<dependency>
    <groupId>cn.gjsm</groupId>
  	<artifactId>openai</artifactId>
  	<version>0.1.1</version>       
</dependency>
```

# 使用 Use
## 基本使用 Basic

```java
// 实例化请求客户端 Instance the OpenAiClient
OpenAiClient openAiClient = OpenAiClientFactory.createClient(OPENAPI_TOKEN);

// 实例化发送的消息 Instance the message
ChatMessage chatMessage = new ChatMessage();
chatMessage.setRole("user");
chatMessage.setContent("今天天气怎么样？");

// 实例化发送的请求 Instance the request
ChatCompletionRequest request = ChatCompletionRequest.builder()
    .messages(Arrays.asList(chatMessage))
    .model("gpt-3.5-turbo")
    .build();

// 执行请求 Execute request
Call<ChatCompletionResponse> chatCompletion = openAiClient.callChatCompletion(request);
Response<ChatCompletionResponse> response = chatCompletion.execute();

// 解析结果 Analysis results
if(response.isSuccessful()){
    System.out.println(JSON.toJSONString(response.body()));
}
```
## 自定义参数 Custom Parameter
```java
OpenAiClient openAiClient =  OpenAiClientFactory.builder()
                .readTimeout(Duration.ofMillis(openAiProperties.getTimeout()))
                .connectTimeout(Duration.ofMillis(openAiProperties.getTimeout()))
                .build()
                .createClient(openAiProperties.getToken());
```

## 代理客户端 Proxy Client
```java
OpenAiClient openAiClient =  OpenAiClientFactory.getInstance()
                .createHttpProxyClient(openAiProperties.getToken(),"代理IP","代理端口");
```

## 动态刷新Token  Refresh Token
框架是直接从环境变量中获取TOKEN，获取的变量名是OPENAPI_TOKEN。
```java
OpenAiClientFactory.refreshToken("您的新TOKEN");
```


# 高级 Advanced

> 在程序启动命令中增加一下参数，可更好的支持本机代理
>
> Add the following parameters to the program startup command to better support the native agent

```shell
-Djava.net.useSystemProxies=true
```

