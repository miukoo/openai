package cn.gjsm.api.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class FileUtil {

    /**
     * 检查dell2的图片格式
     * @param file
     * @param file
     */
    public static void checkImage(File file){
        try {
            String name = file.getName().toLowerCase();
            if(!name.endsWith("png")){
                throw new RuntimeException("上传的图片只能是png (only png)");
            }
            BufferedImage bufferedImage = ImageIO.read(file);
            int width = bufferedImage.getWidth();
            int height = bufferedImage.getHeight();
            if(width!=height){
                throw new RuntimeException("上传的图片必须是正方形 (Image width must be equal to height)");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
