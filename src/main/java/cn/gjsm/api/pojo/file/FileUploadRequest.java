package cn.gjsm.api.pojo.file;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import java.io.File;

@Data
@Builder
public class FileUploadRequest {

    @NonNull
    File file;
    @Builder.Default
    boolean check = true;
    @Builder.Default
    String purpose="fine-tune";

    public RequestBody toRequestBody(){
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        if(file!=null){
            RequestBody imageBody = RequestBody.create(MediaType.parse("application/json"),file);
            builder.addFormDataPart("file",file.getName(),imageBody);
        }
        builder.addFormDataPart("purpose",getPurpose());
        return builder.build();
    }

}
