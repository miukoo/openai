package cn.gjsm.api.pojo.file;

import lombok.Data;

@Data
public class FileObject {
    String id;
    String object;
    Long bytes;
    Long createdAt;
    String filename;
    String purpose;
}
