package cn.gjsm.api.pojo.file;

import lombok.Data;

@Data
public class FileObjectDeleteResult {
    String id;
    String object;
    boolean deleted;
}
