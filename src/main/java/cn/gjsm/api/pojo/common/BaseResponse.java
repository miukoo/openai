package cn.gjsm.api.pojo.common;

import lombok.Data;

import java.util.List;


/**
 * 基础响应类型
 * @author miukoo mailto:miukoo@126.com
 */
@Data
public class BaseResponse<T> {
    public List<T> data;
    public String object;
}
