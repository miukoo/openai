package cn.gjsm.api.pojo.common;

import lombok.Data;


/**
 * 基础响应类型
 * @author miukoo mailto:miukoo@126.com
 */
@Data
public class BaseCompletionResponse {
    String id;
    String object;
    long created;
    String model;
    Usage usage;
}
