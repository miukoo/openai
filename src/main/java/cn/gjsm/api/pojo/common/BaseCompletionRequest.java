package cn.gjsm.api.pojo.common;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Map;


/**
 * 基础请求参数类型
 * @author miukoo mailto:miukoo@126.com
 */
@Data
@SuperBuilder
public class BaseCompletionRequest {
    String model;
    @Builder.Default
    double temperature=1;
    @Builder.Default
    double topP = 1;
    @Builder.Default
    int n = 1;
    @Builder.Default
    boolean stream = false;
    List<String> stop;
    @Builder.Default
    int maxTokens = 512;
    @Builder.Default
    double presencePenalty=0;
    @Builder.Default
    double frequencyPenalty=0;
    String user;
    Map<String, Integer> logitBias;
}
