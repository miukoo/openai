package cn.gjsm.api.pojo.common;

import lombok.Data;

/**
 * 统计类
 * @author miukoo mailto:miukoo@126.com
 */
@Data
public class Usage {
    long promptTokens;
    long completionTokens;
    long totalTokens;
}
