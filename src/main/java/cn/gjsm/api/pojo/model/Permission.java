package cn.gjsm.api.pojo.model;

import lombok.Data;

@Data
public class Permission {
    public String id;
    public String object;
    public long created;
    public boolean allowCreateEngine;
    public boolean allowSampling;
    public boolean allowLogProbs;
    public boolean allowSearchIndices;
    public boolean allowView;
    public boolean allowFineTuning;
    public String organization;
    public String group;
    public boolean isBlocking;
}
