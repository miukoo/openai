package cn.gjsm.api.pojo.model;

import lombok.Data;

import java.util.List;

@Data
public class Model {
    public String id;
    public String object;
    public String ownedBy;
    public List<Permission> permission;
    public String root;
    public String parent;
}
