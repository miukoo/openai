package cn.gjsm.api.pojo.moderation;

import lombok.Data;

import java.util.List;

@Data
public class ModerationResult {
    public String id;
    public String model;
    public List<Moderation> results;
}
