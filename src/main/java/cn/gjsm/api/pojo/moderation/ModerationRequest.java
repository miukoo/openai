package cn.gjsm.api.pojo.moderation;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class ModerationRequest {
    @NonNull
    String input;
    String model;
}
