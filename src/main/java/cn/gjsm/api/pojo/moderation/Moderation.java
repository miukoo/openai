package cn.gjsm.api.pojo.moderation;

import cn.gjsm.api.openai.OpenAiClientFactory;
import lombok.Data;

@Data
public class Moderation {
    public boolean flagged;
    public ModerationCategories categories;
    public ModerationCategoryScores categoryScores;
}
