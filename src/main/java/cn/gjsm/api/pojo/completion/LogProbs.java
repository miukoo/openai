//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package cn.gjsm.api.pojo.completion;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class LogProbs {
    List<String> tokens;
    List<Double> tokenLogprobs;
    List<Map<String, Double>> topLogprobs;
    List<Integer> textOffset;
}
