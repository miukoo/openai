package cn.gjsm.api.pojo.completion;

import cn.gjsm.api.pojo.common.BaseCompletionResponse;
import lombok.Data;

import java.util.List;

/**
 * AI响应结果
 * @author miukoo mailto:miukoo@126.com
 */
@Data
public class CompletionResponse extends BaseCompletionResponse {
    List<CompletionChoice> choices;
}
