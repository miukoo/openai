package cn.gjsm.api.pojo.completion;

import lombok.Data;

@Data
public class CompletionChoice {
    String text;
    Integer index;
    LogProbs logprobs;
    String finishReason;
}
