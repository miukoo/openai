package cn.gjsm.api.pojo.completion;

import cn.gjsm.api.pojo.chat.ChatMessage;
import cn.gjsm.api.pojo.common.BaseCompletionRequest;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * AI对话请求结果
 * @author miukoo mailto:miukoo@126.com
 */
@Data
@SuperBuilder
public class CompletionRequest extends BaseCompletionRequest {

    List<String> prompt;
    String suffix;
    Integer logprobs;
    @Builder.Default
    String model = "text-davinci-003";
    @Builder.Default
    boolean echo = false;
    @Builder.Default
    int bestOf = 1;

}
