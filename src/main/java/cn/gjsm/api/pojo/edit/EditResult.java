package cn.gjsm.api.pojo.edit;

import cn.gjsm.api.pojo.common.Usage;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
public class EditResult {
    public String object;
    public long created;
    public List<EditChoice> choices;
    public Usage usage;
}
