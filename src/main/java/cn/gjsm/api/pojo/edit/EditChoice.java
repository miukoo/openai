package cn.gjsm.api.pojo.edit;

import lombok.Data;

@Data
public class EditChoice {
    String text;
    Integer index;
}
