package cn.gjsm.api.pojo.edit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class EditRequest {
    String model;
    String input;
    @NonNull
    String instruction;
    @Builder.Default
    double temperature=1;
    @Builder.Default
    double topP = 1;
    @Builder.Default
    int n = 1;
}
