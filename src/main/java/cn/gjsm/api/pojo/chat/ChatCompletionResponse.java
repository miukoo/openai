package cn.gjsm.api.pojo.chat;

import cn.gjsm.api.pojo.common.BaseCompletionResponse;
import lombok.Data;

import java.util.List;

/**
 * GPT3.5响应结果
 * @author miukoo mailto:miukoo@126.com
 */
@Data
public class ChatCompletionResponse extends BaseCompletionResponse {
    List<ChatCompletionChoice> choices;
}
