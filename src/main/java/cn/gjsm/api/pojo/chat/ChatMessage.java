package cn.gjsm.api.pojo.chat;

import lombok.Data;

/**
 * GPT3.5对话记录
 * @author miukoo mailto:miukoo@126.com
 */
@Data
public class ChatMessage {
    String role;
    String content;
}
