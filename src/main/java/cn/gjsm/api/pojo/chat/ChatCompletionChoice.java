
package cn.gjsm.api.pojo.chat;

import lombok.Data;

/**
 * GPT3.5响应结果
 * @author miukoo mailto:miukoo@126.com
 */
@Data
public class ChatCompletionChoice {
    Integer index;
    ChatMessage message;
    String finishReason;
}
