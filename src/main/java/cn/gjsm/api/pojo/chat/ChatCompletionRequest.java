package cn.gjsm.api.pojo.chat;

import cn.gjsm.api.pojo.common.BaseCompletionRequest;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * GPT3.5请求结果
 * @author miukoo mailto:miukoo@126.com
 */
@Data
@SuperBuilder
public class ChatCompletionRequest extends BaseCompletionRequest {

    List<ChatMessage> messages;
    @Builder.Default
    String model = "gpt-3.5-turbo";

}
