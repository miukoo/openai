package cn.gjsm.api.pojo.fine.tunes;

import lombok.Data;

@Data
public class FineTuneEvent {
    String object;
    Long createdAt;
    String level;
    String message;
}
