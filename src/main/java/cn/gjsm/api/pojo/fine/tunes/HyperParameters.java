package cn.gjsm.api.pojo.fine.tunes;

import lombok.Data;

@Data
public class HyperParameters {
    String batchSize;
    Double learningRateMultiplier;
    Integer nEpochs;
    Double promptLossWeight;
}
