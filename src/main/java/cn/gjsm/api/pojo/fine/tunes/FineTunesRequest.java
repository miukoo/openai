package cn.gjsm.api.pojo.fine.tunes;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
@Builder
public class FineTunesRequest {

    @NonNull
    String trainingFile;
    String validationFile;
    @Builder.Default
    String model = "ada";
//    @Builder.Default
//    Integer nEpochs = 4;
    Integer batchSize;
    Double learningRateMultiplier;
    @Builder.Default
    Double promptLossWeight = 0.01;
    @Builder.Default
    Boolean computeClassificationMetrics = false;
    Integer classificationNClasses;
    String classificationPositiveClass;
    List<Double> classificationBetas;
    String suffix;

}
