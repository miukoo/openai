package cn.gjsm.api.pojo.fine.tunes;

import lombok.Data;

@Data
public class DeleteFineTuneResult {
    String id;
    String object;
    boolean deleted;
}
