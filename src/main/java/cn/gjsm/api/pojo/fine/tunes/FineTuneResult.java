package cn.gjsm.api.pojo.fine.tunes;

import cn.gjsm.api.pojo.file.FileObject;
import lombok.Data;

import java.util.List;

@Data
public class FineTuneResult {
    String id;
    String object;
    String model;
    Long createdAt;
    List<FineTuneEvent> events;
    String fineTunedModel;
    HyperParameters hyperparams;
    String organizationId;
    List<FileObject> resultFiles;
    String status;
    List<FileObject> trainingFiles;
    Long updatedAt;
    List<FileObject> validationFiles;
}
