package cn.gjsm.api.pojo.embedding;

import cn.gjsm.api.pojo.common.Usage;
import lombok.Data;

import java.util.List;

@Data
public class EmbeddingResult {
    String model;
    String object;
    List<Embedding> data;
    Usage usage;
}
