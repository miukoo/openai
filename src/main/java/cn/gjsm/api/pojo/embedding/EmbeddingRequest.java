package cn.gjsm.api.pojo.embedding;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
@Builder
public class EmbeddingRequest {
    String model;
    @NonNull
    List<String> input;
    String user;
}
