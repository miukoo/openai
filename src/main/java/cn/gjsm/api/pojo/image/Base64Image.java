package cn.gjsm.api.pojo.image;

import lombok.Data;

@Data
public class Base64Image {
    String url;
    String b64Json;
}
