package cn.gjsm.api.pojo.image;

import cn.gjsm.api.util.FileUtil;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Data
@Builder
public class ImageEditRequest {

    @NonNull
    File image;
    File mask;
    @Builder.Default
    boolean check = true;
    @NonNull
    String prompt;
    @Builder.Default
    String size="512x512";
    @Builder.Default
    String responseFormat="url";
    String user;
    @Builder.Default
    int n = 1;

    public RequestBody toRequestBody(){
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        if(image!=null){
            if(check){
                FileUtil.checkImage(image);
            }
            RequestBody imageBody = RequestBody.create(MediaType.parse("image/png"),image);
            builder.addFormDataPart("image",image.getName(),imageBody);
        }
        if(mask!=null){
            if(check){
                FileUtil.checkImage(mask);
            }
            RequestBody imageBody = RequestBody.create(MediaType.parse("image/png"),mask);
            builder.addFormDataPart("mask",mask.getName(),imageBody);
        }
        builder.addFormDataPart("prompt",getPrompt());
        builder.addFormDataPart("n",""+getN());
        builder.addFormDataPart("size",getSize());
        builder.addFormDataPart("response_format",getResponseFormat());
        if(getUser()!=null) {
            builder.addFormDataPart("user", getUser());
        }
        return builder.build();
    }

}
