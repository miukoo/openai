package cn.gjsm.api.pojo.image;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class ImageRequest {
    @NonNull
    String prompt;
    @Builder.Default
    String size="512x512";
    @Builder.Default
    String responseFormat="url";
    String user;
    @Builder.Default
    int n = 1;

}
