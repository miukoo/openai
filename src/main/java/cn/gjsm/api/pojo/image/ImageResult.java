package cn.gjsm.api.pojo.image;

import lombok.Data;

import java.util.List;

@Data
public class ImageResult {
    Long createdAt;
    List<Base64Image> data;
}
