package cn.gjsm.api.openai;

import cn.gjsm.api.common.factory.BaseClientFactory;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;

import java.io.IOException;

/**
 * 定义OpenAi客户端创建工程
 *
 * @author miukoo mailto:miukoo@126.com
 */
@Data
@NoArgsConstructor
@SuperBuilder
public class OpenAiClientFactory extends BaseClientFactory {

    static OpenAiClientFactory INSTANCE = new OpenAiClientFactory();

    String baseUrl = "https://api.openai.com/";

    public static OpenAiClient createClient(String token){
        System.setProperty("OPENAPI_TOKEN",token);
        return INSTANCE.createClient();
    }

    public static OpenAiClient defaultClient(){
        return INSTANCE.createClient();
    }

    public static OpenAiClientFactory getInstance(){
        return INSTANCE;
    }

    public OpenAiClient createHttpProxyClient(String token,String host,int port){
        System.setProperty("OPENAPI_TOKEN",token);
        return INSTANCE.createHttpProxyClient(host,port);
    }

    public OpenAiClient createSocketProxyClient(String token,String host,int port){
        System.setProperty("OPENAPI_TOKEN",token);
        return INSTANCE.createSocketProxyClient(host,port);
    }

    public static void refreshToken(String token){
        System.setProperty("OPENAPI_TOKEN",token);
    }


    public Retrofit.Builder defualtRetrofitBuilder(){
        Retrofit.Builder builder = super.defualtRetrofitBuilder();
        builder.baseUrl(baseUrl);
        return builder;
    }

    public OkHttpClient.Builder defaultUnsafeOkHttpClient() {
        OkHttpClient.Builder builder = super.defaultUnsafeOkHttpClient();
        builder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                String token = System.getProperty("OPENAPI_TOKEN");
                if(token!=null) {
                    Request request = chain.request().newBuilder()
                            .addHeader("Connection","close")
                            .header("Authorization", "Bearer " + token).build();
                    return chain.proceed(request);
                }else{
                    return chain.proceed(chain.request());
                }
            }
        });
        return builder;
    }


}
