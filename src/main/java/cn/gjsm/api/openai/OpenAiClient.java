package cn.gjsm.api.openai;

import cn.gjsm.api.pojo.chat.ChatCompletionRequest;
import cn.gjsm.api.pojo.chat.ChatCompletionResponse;
import cn.gjsm.api.pojo.common.BaseResponse;
import cn.gjsm.api.pojo.completion.CompletionRequest;
import cn.gjsm.api.pojo.completion.CompletionResponse;
import cn.gjsm.api.pojo.edit.EditRequest;
import cn.gjsm.api.pojo.edit.EditResult;
import cn.gjsm.api.pojo.embedding.EmbeddingRequest;
import cn.gjsm.api.pojo.embedding.EmbeddingResult;
import cn.gjsm.api.pojo.file.FileObject;
import cn.gjsm.api.pojo.file.FileObjectDeleteResult;
import cn.gjsm.api.pojo.fine.tunes.DeleteFineTuneResult;
import cn.gjsm.api.pojo.fine.tunes.FineTuneEvent;
import cn.gjsm.api.pojo.fine.tunes.FineTuneResult;
import cn.gjsm.api.pojo.fine.tunes.FineTunesRequest;
import cn.gjsm.api.pojo.image.ImageRequest;
import cn.gjsm.api.pojo.image.ImageResult;
import cn.gjsm.api.pojo.model.Model;
import cn.gjsm.api.pojo.moderation.ModerationRequest;
import cn.gjsm.api.pojo.moderation.ModerationResult;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * 定义OpenAi的接口
 *
 * @author miukoo mailto:miukoo@126.com
 */
public interface OpenAiClient {

    @POST("v1/chat/completions")
    Call<ChatCompletionResponse> callChatCompletion(@Body ChatCompletionRequest request);

    @POST("v1/completions")
    Call<CompletionResponse> callCompletion(@Body CompletionRequest request);

    @GET("v1/models")
    Call<BaseResponse<Model>> listModels();

    @GET("/v1/models/{modelId}")
    Call<Model> getModelById(@Path("modelId") String modelId);

    @POST("/v1/edits")
    Call<EditResult> callEdit(@Body EditRequest request);

    @POST("/v1/embeddings")
    Call<EmbeddingResult> callEmbeddings(@Body EmbeddingRequest request);

    @GET("/v1/files")
    Call<BaseResponse<FileObject>> listFiles();

    /**
     * 上传文件
     * 可以通过FileUploadRequest构建请求，并调用toRequestBody()转为request请求
     * @param request
     * @return
     */
    @POST("/v1/files")
    Call<FileObject> uploadFile(@Body RequestBody request);

    @DELETE("/v1/files/{fileId}")
    Call<FileObjectDeleteResult> deleteFile(@Path("fileId") String fileId);

    @GET("/v1/files/{fileId}")
    Call<FileObject> getFileObjectById(@Path("fileId") String fileId);

    /**
     * 通过提示生成新图片
     * @param request
     * @return
     */
    @POST("/v1/images/generations")
    Call<ImageResult> callImage(@Body ImageRequest request);

    /**
     * 通过提示修改图片
     * 可以通过ImageEditRequest构建请求，并调用toRequestBody()转为request请求
     * @param request
     * @return
     */
    @POST("/v1/images/edits")
    Call<ImageResult> callImageEdit(@Body RequestBody request);

    /**
     * 通过提示修改图片
     * 可以通过ImageVariationsRequest构建请求，并调用toRequestBody()转为request请求
     * @param request
     * @return
     */
    @POST("/v1/images/variations")
    Call<ImageResult> callImageVariation(@Body RequestBody request);

    @POST("/v1/moderations")
    Call<ModerationResult> callModeration(@Body ModerationRequest request);

    /**
     * 创建一个自定义微调模型
     * @param request
     * @return
     */
    @POST("/v1/fine-tunes")
    Call<FineTuneResult> callFineTunes(@Body FineTunesRequest request);

    /**
     * 查看自定义微调模型列表
     * @return
     */
    @GET("/v1/fine-tunes")
    Call<BaseResponse<FineTuneResult>> listFineTunes();

    @GET("/v1/fine-tunes/{fine_tune_id}")
    Call<FineTuneResult> retrieveFineTune(@Path("fine_tune_id") String fineTuneId);

    @GET("/v1/fine-tunes/{fine_tune_id}/cancel")
    Call<FineTuneResult> cancelFineTune(@Path("fine_tune_id") String fineTuneId);

    /**
     * 查看模型的进度
     * @param fineTuneId
     * @return
     */
    @GET("/v1/fine-tunes/{fine_tune_id}/events")
    Call<BaseResponse<FineTuneEvent>> listFineTuneEvents(@Path("fine_tune_id") String fineTuneId);

    @DELETE("/v1/models/{fine_tune_id}")
    Call<DeleteFineTuneResult> deleteFineTune(@Path("fine_tune_id") String fineTuneId);
}
